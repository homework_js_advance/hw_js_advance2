"use strict"

const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];

  // Отримати елемент з id="root"
  const rootElement = document.getElementById("root");

  // Створити елемент ul
  const ulElement = document.createElement("ul");

  // Функція для перевірки об'єкта на коректність
  function isValidBook(book) {
    if (!book || !book.author || !book.name || !book.price) {
      return false;
    }
    return true;
  }

  // Пройтися по кожній книзі і створити елемент li для кожної
  books.forEach(book => {
    // Перевірка на коректність
    if (isValidBook(book)) {
      const liElement = document.createElement("li");
      liElement.textContent = `${book.author ? book.author + " - " : ""}${book.name} (${book.price || 'Ціна не вказана'})`;
      ulElement.appendChild(liElement);
    }
  });

  // Додати елемент ul до елемента з id="root"
  rootElement.appendChild(ulElement);